![switching.social](static/images/switchingsoftware_small.jpg)

_Ethical, easy-to-use and privacy-conscious alternatives to well-known software_

[Website][swiso] | [Contribution Guidelines][contrib] | [Code of Conduct][coc] | [License][license] | <a rel="me" href="https://fedifreu.de/@switchingsoftware">Mastodon</a>

[swiso]: https://switching.software
[contrib]: https://codeberg.org/swiso/website/src/branch/main/CONTRIBUTING.md
[coc]: https://codeberg.org/swiso/website/src/branch/main/CODE_OF_CONDUCT.md
[license]: https://creativecommons.org/licenses/by-sa/4.0/

## Contribute

We highly **appreciate your proposals** and ideas for new
content. Please refer to [our Contribution Guidelines][contrib] before
submitting issues or pull requests.

As you might notice, this project has a lot of content. So please,
have a look around and [open an issue][open issue] in case you find
mistakes, broken links or deprecated information.

[open issue]: https://codeberg.org/swiso/website/issues/new

## Developing

Please refer to [this
guide](https://gohugo.io/getting-started/installing/) to install Hugo
for local development.

Current hugo version on the server is `v0.111.3`.

## License

[Switching.software][swiso] is based on the work of
[switching.social][switching.social archive].

[switching.social archive]: https://web.archive.org/web/20190915101437/https://switching.social/

All content was, is and will always be licensed as [CC BY-SA
4.0][license]. Excluded are the logos/icons of the entries, which are
included under [Fair Use](https://en.wikipedia.org/wiki/Fair_use) and
keep their original licensing.

## Thanks

This site is only possible thanks to projects like:

- [switching.social][switching.social archive]: The original site,
  that went offline in September 2019, but was restored
  [here](https://codeberg.org/swiso-en/archive)
- [Hugo](https://gohugo.io/): A powerful open-source static site
  generator
- [Sprectre.css](https://picturepan2.github.io/spectre/getting-started.html):
  A lightweight and responsive CSS framework
- [Mastodon](https://joinmastodon.org/): A federated software running
  our main [social media
  account](https://fedifreu.de/@switchingsoftware)
- [Codeberg.org](https://codeberg.org/): A non-profit and
  non-government organization, that gives our codebase and website a
  home – powered by [Forgejo](https://forgejo.org/)

