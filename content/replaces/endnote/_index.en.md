---
title: EndNote
subtitle: Referential Tool
aliases:
    - /ethical-alternatives-to-endnote/
---

EndNote is a reference management software used by academics to organize references and create bibliographies. Only the online version is free of charge. License purchase is needed after the 30 day free trial. EndNote is also not available on some versions of LibreOffice.