---
title: Amazon Goodreads
subtitle: Social Reading Platform
provider: amazon
order:
    - bookwyrm
    - open-library
    - libreture
    - inventaire
aliases:
    - /ethical-alternatives-to-goodreads/
---

Goodreads is the most popular social reading platform. However, it is
owned by Amazon and therefore is [equally problematic][amazon] as
every other service provided by Amazon.

[amazon]: {{< relref "/replaces/amazon-books" >}}

