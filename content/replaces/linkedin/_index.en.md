---
title: LinkedIn
subtitle: Business Social Network
provider: microsoft
order:
    - friendica
    - guppe
---

LinkedIn is Microsoft's proprietary, centralized, for-profit social network for
business.
