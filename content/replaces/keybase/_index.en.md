---
title: Keybase
subtitle: Identity Directory
order:
    - wheretofind-me
    - keyoxide
---

In May 2020 Keybase was [acquired][keybase-acquired] by Zoom.

[keybase-acquired]: https://keybase.io/blog/keybase-joins-zoom
