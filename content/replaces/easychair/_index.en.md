---
title: EasyChair
subtitle: Conference Management
---

EasyChair is developed by a for-profit corporation using the freemium business
model. Its source code is not available to its users, despite this tool being
often used for academic purposes.
