---
title: Facebook
subtitle: Social Networks
provider: facebook
order: 
    - mastodon
    - friendica
    - mobilizon
    - guppe
aliases:
    - /ethical-alternatives-to-facebook/
---
