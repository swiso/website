---
title: Facebook
subtitle: Soziale Netzwerke
provider: facebook
order: 
    - mastodon
    - friendica
    - mobilizon
    - guppe
aliases:
    - /ethical-alternatives-to-facebook/
---
