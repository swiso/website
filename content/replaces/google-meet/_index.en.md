---
title: Google Meet
subtitle: Video Conferencing
provider: google
order:
    - jitsi-meet
    - bigbluebutton
    - jami  
aliases:
    - /ethical-alternatives-to-meet/
---