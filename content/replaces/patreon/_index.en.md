---
title: Patreon
subtitle: Donation Platforms
order:
    - liberapay
    - open-collective
aliases:
    - /ethical-alternatives-to-patreon/
---

Patreon is a closed-source platform which holds your content and your
supporters captive.
