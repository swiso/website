---
title: Adobe Acrobat
subtitle: PDF Management
order:
  - xournalpp
  - okular
provider: adobe
---

All members of the Adobe Acrobat suite are proprietary software. These products
are also not available on any Linux system.
