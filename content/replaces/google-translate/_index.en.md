---
title: Google Translate
subtitle: Translators
provider: google
order: 
    - deepl
    - libretranslate
    - apertium
aliases:
    - /ethical-alternatives-to-translate/
---
Like all Google tools, Google Translate doesn't protect the users' privacy. The company is allowed to use any information that was entered in the translating field. This tool should not be used for confidential data. 

Google Translate is also known to miss the point sometimes, either by allowing easy-to-avoid mistakes, or by not understanding the context or the tone of the text. 

However, so far, there are no alternatives to Google Translate's translation from images using Google Lens. 