---
title: Good Old Games (GOG)
icon: icon.svg
replaces:
    - steam
---

**GOG** is a Steam-like video games platform without any Digital
Rights Management: games you buy are yours forever, no one can take
them away from you.

GOG has become well-known for selling classic older games that have
been fixed to work on modern computers, but they also sell some more
modern titles too.

There’s a games management app called GOG Galaxy, but it’s totally
optional. GOG games can be bought and used without GOG Galaxy.

{{< infobox >}}
- **Website:** 
    - [GOG.com](https://www.gog.com/)
- **Download:**
    - [GOG Galaxy](https://www.gog.com/galaxy) (Windows)
{{< /infobox >}}

