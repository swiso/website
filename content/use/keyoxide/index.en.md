---
title: Keyoxide
icon: icon.svg
replaces:
    - keybase
---

**Keyoxide** is a [free and open source][floss] "tool to create and
verify decentralized online identities". It allows you to create a
small profile on which you can reference different web resources you
control, e.g. domains, social media profiles, etc such that others can
find you on various platforms. And Keyoxide does that in a way which
allows others to verify that they are, in fact, talking to you and not
an imposter.

{{< infobox >}}
- **Website:**
    - [keyoxide.org](https://keyoxide.org/)
- **Android app:**
    - [keyoxide (F-Droid)](https://f-droid.org/packages/org.keyoxide.keyoxide/)
    - [keyoxide (Google Play Store)](https://play.google.com/store/apps/details?id=org.keyoxide.keyoxide)
- **iOS app:**
    - [keyoxide](https://apps.apple.com/us/app/keyoxide/id1670664318)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
