---
title: CryptPad
icon: icon.svg
replaces:
    - doodle
    - evernote
    - google-drive
    - microsoft-office
    - google-forms
---

**CryptPad** is a collection of [open source][floss] and
privacy-friendly collaboration tools. It's a complete online office
suite that is end-to-end encrypted, no one except you (and whoever you
give your access keys to) can access your data. It lets you easily
create rich text and Markdown documents, presentations, spreadsheets,
poll, forms & kanban in your web browser.

Everyone can host their own instance following the Administrator
Guide. However, for less tech-savvy people, you can use the flagship
instance, maintained by the developers of the project. The service is
free up to a certain storage limit, with paid accounts for additional
storage. You can even use the free service without registering if you
want to. It’s worth registering though, as you can keep your documents
permanently online this way.

{{< infobox >}}
- **Project website:**
    - [cryptpad.org](https://cryptpad.org)
- **Instances:**
    - [cryptpad.fr](https://cryptpad.fr) (flagship)
	- [cryptpad.digitalcourage.de](https://cryptpad.digitalcourage.de/)
- **Documentation:**
    - [docs.cryptpad.org](https://docs.cryptpad.org)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
