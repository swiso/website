---
title: Delightful lists
icon: icon.svg
replaces: 
    - switching-software
---

**Delightful lists** is a collection of lists on free software, open
science and information sources. It is maintained by the Humane Tech
Community.

{{< infobox >}}
- **Website:** 
    - [Delightful lists](https://codeberg.org/teaserbot-labs/delightful)
    - [Community](https://humanetech.community/)
{{< /infobox >}}
