---
title: Liberaforms
icon: icon.png
replaces:
    - google-forms
---

**Liberaforms** is a [free and open source][floss] survey
software. You can either use free instances on the web or self-host
it.

{{< infobox >}}
- **Website:** 
    - [Liberaforms.org](https://liberaforms.org/en)
- **Source code**: https://gitlab.com/liberaforms/liberaforms
- **Instances**:
    - https://my.liberaforms.org/my-form
    - https://usem.liberaforms.org/my-form
    - https://erabili.liberaforms.org/my-form
    - https://forms.komun.org/
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
