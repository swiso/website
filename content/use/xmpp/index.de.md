---
title: XMPP
icon: icon.svg
replaces:
    - whatsapp
    - facebook-messenger
---

**XMPP** (früher Jabber) ist ein offenes und weit verbreitetes Kommunikationsprotokoll. Die Nutzung ist einfach: Registriere Dich auf einem der XMPP-Server und installiere eine der XMPP-Apps.

Aufgrund der dezentralen Funktionsweise gibt es viele Seiten, auf denen Du Dich registrieren kannst. Deine Freunde müssen nicht den gleichen Server wählen wie Du: Alle Konten auf allen Seiten können miteinander kommunizieren

Bei der Registrierung kannst Du Deinen Usernamen frei wählen. Eine Handynummer wird nicht benötigt. Alternativ gibt es mit Quicksy eine XMPP-Variante auf Basis der Handynummer.

{{< infobox >}}
- **Registrierung:** 
    - [wiuwiu.de](https://wiuwiu.de/)
    - [simplewire.de](https://simplewire.de/register.html)
    - und [viele](https://www.freie-messenger.de/sys_xmpp/server/#empfehlenswerte-deutschsprachige-server) [weitere](https://list.jabber.at/)
- **Android-App:** 
    - [Conversations (F-Droid)](https://f-droid.org/en/packages/eu.siacs.conversations/)
    - [Conversations (Google Play Store)](https://play.google.com/store/apps/details?id=eu.siacs.conversations) 
- **iOS-Apps:** 
    - [ChatSecure](https://apps.apple.com/app/chatsecure/id464200063)
    - [Monal IM](https://itunes.apple.com/app/monal-free-xmpp-chat/id317711500)
    - [Siskin.im](https://apps.apple.com/app/tigase-messenger/id1153516838)
- **Desktop-Apps:** 
    - [Pidgin](https://pidgin.im/) (any)
    - [Gajim](https://gajim.org/downloads.php?lang=de) (Windows/Linux)
{{< /infobox >}}