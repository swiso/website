---
title: Luanti
icon: icon.svg
replaces:
    - minecraft
---

**Luanti** is a free [open source][floss] Minecraft-style game with huge worlds, a variety of different playing modes, online and offline gameplay, multiplayer servers, lots of user-created mods and more.

{{< infobox >}}
- **Website:**
    - [Luanti.org](https://www.luanti.org/)
- **Desktop:**
    - [Windows / macOS / Linux](https://www.luanti.org/downloads/)
- **Android app:**
    - [Luanti (F-Droid)](https://f-droid.org/packages/net.minetest.minetest/)
    - [Luanti (Google Play Store)](https://play.google.com/store/apps/details?id=net.minetest.minetest)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
