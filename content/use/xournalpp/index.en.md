---
title: Xournal++
icon: icon.svg
replaces:
    - adobe-acrobat
    - evernote
    - google-keep
    - microsoft-onenote
---

**Xournal++** (/ˌzɚnl̟ˌplʌsˈplʌs/) is a FLOSS note-taking software for PDF
documents. It is a modern rewrite of
[Xournal](https://sourceforge.net/projects/xournal/) which is extremely
featureful.

{{< infobox >}}
- **Website:**
    - [xournalpp.github.io](https://xournalpp.github.io/)
    - [Matrix](https://gitter.im/xournalpp/xournalpp)
    - [Source](https://github.com/xournalpp/xournalpp/)
{{< /infobox >}}
