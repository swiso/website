---
title: Syncthing
icon: icon.svg
replaces: 
    - google-drive
    - dropbox
---

**Syncthing** is a program to easily synchronize folders across two or more computers. Instead of uploading your files to a central server for sharing them, Syncthing sends files directly from one computer to the other.

To do this, Syncthing assigns an ID to the computer it is running on. Share this ID with your friends, or enter it on your other devices, to sync folders. After confirmation on both sides, changes in a folder are synced in real time.

{{< infobox >}}
- **Website:** 
    - [Syncthing](https://syncthing.net/)
- **Android app:**
    - [Syncthing-Fork (F-Droid)](https://f-droid.org/packages/com.github.catfriend1.syncthingandroid/)
    - [Syncthing-Fork (Google Play Store)](https://play.google.com/store/apps/details?id=com.github.catfriend1.syncthingandroid) 
- **Desktop app:**
    - [Windows / macOS / Linux](https://syncthing.net/downloads/)
{{< /infobox >}}
