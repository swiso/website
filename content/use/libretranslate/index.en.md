---
title: LibreTranslate
icon: icon.png
replaces:
    - google-translate
---

**LibreTranslate** is a [free/libre][floss] translator available online and self-hosted. The translation engine is powered by the open source Argos Translate library.

Also works off-line for more advanced users. 

{{< infobox >}}
- **Website:**
    - [LibreTranslate.com](https://libretranslate.com/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}