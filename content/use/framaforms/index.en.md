---
title: Framaforms
icon: icon.svg
replaces:
    - google-forms
---

**Framaforms** is an online survey service provided by the French
[Framasoft]({{< ref "/use/framasoft" >}})
collective based on the [free and open source][floss]
[Yakforms](https://framagit.org/yakforms/yakforms_org) software.

{{< infobox >}}
- **Website**: 
  - [framaforms.org](https://framaforms.org/abc/en/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
