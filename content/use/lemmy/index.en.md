---
title: Lemmy
icon: icon.svg
replaces:
    - reddit
---

**Lemmy** is a [libre][floss] and [federated][fediverse] link
aggregation and discussion platform.

{{< infobox >}}
- **Instances:**
    - [lemmy.ml](https://lemmy.ml/)
	- [lemmy.world](https://lemmy.world/)
	- [sh.itjust.works](https://sh.itjust.works/)
	- [instance list](https://join-lemmy.org/)
- **Source code:**
    - https://github.com/dessalines/lemmy
- **Mobile clients (Android):**
  - [Jerboa (F-Droid)](https://f-droid.org/packages/com.jerboa/)
  - [Jerboa (Google Play Store)](https://play.google.com/store/apps/details?id=com.jerboa&hl=en_US)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
[fediverse]: {{< relref "/articles/federated-sites" >}}
