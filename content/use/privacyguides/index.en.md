---
title: PrivacyGuides
icon: icon.svg
replaces:
    - switching-software
---

**PrivacyGuides** is a non-profit organization dedicated to strengthening online privacy and aims to help people strengthen their privacy and online activities. It maintains a central security resource and repository of information including software recommendations and guides.

Their focus is transparency: Every software recommendation is open for discussion, every change to the website can be retraced.

{{< infobox >}}
- **Website:**
    - [PrivacyGuides.org](https://www.privacyguides.org)
    - [Discourse](https://discuss.privacyguides.net)
    - [Mastodon account](https://mastodon.neat.computer/@privacyguides)
    - [Contributing](https://github.com/privacyguides/privacyguides.org)
    - [Matrix](https://matrix.to/#/#privacyguides:matrix.org)
{{< /infobox >}}
