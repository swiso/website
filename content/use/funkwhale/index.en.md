---
title: Funkwhale
icon: icon.svg
replaces:
    - soundcloud
---

**Funkwhale** is a [libre][floss] and [federated][fediverse] music platform,
similar to Grooveshark or SoundCloud. It lets you easily discover and share
music or podcasts online through a music-oriented social network.

{{< infobox >}}
- **Website:**
    - [Funkwhale.audio](https://funkwhale.audio/)
- **Source code:**
    - <https://dev.funkwhale.audio/funkwhale/funkwhale>
- **List of instances:**
    - <https://www.funkwhale.audio/join/>
{{< /infobox >}}

[fediverse]: {{< relref "/articles/federated-sites" >}}
[floss]: {{< relref "/articles/free-libre-open-software" >}}
