---
title: Mobilizon
icon: icon.svg
lists:
    - meetup
    - facebook
---

**Mobilizon** is a [federated][fediverse] and [libre][floss] event organization platform, provided by France’s largest ethical software organisation Framasoft. 


{{< infobox >}}
- **Website:**
    - [JoinMobilizon.org](https://joinmobilizon.org/en/)
    - [List of instances](https://instances.joinmobilizon.org/)
- **Android app:**
    - [Mobilizon (F-Droid)](https://f-droid.org/en/packages/app.fedilab.mobilizon/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
[fediverse]: {{< relref "/articles/federated-sites" >}}
