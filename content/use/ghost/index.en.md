---
title: Ghost
icon: icon.png
replaces:
    - substack
    - medium
    - patreon
---

A [free and open-source][floss] publishing platform which integrates
newsletter subscriptions and payment options. Can be used as a service or be
self-hosted.

{{< infobox >}}
- **Project website:**
    - <https://ghost.org/>
- **Source code:**
    - <https://github.com/TryGhost/Ghost>
- **Documentation:**
    - <https://ghost.org/docs/>
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
