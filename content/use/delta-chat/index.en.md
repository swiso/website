---
title: Delta Chat
icon: icon.svg
replaces: 
    - whatsapp
    - facebook-messenger
---

**Delta Chat** is a FOSS, decentralized and secure instant messaging app based
on email protocols. Delta Chat feels like Whatsapp or Telegram but you can also
use and regard it as an e-mail app. You may use classic e-mail servers and an
existing e-mail account, without the need to sign up to any platform. Your
messages will be displayed on other users' email clients. When they email you
back, you will see their reply in the same chat. If you don't want your
messages/emails to be sent from your own email address, you can also send them
anonymously from a public "chatmail server" (minimal e-mail servers designed for
fast and secure operations).

{{< infobox >}}
- **Website:** 
    - [delta.chat](https://delta.chat/)
    - [Mastodon](https://chaos.social/@delta)
    - [RSS](https://delta.chat/feed.xml)
    - [Forum](https://support.delta.chat/)
    - [Blog](https://delta.chat/en/blog)
    - [Source](https://github.com/deltachat/)
- **Download:**
    - [Delta Chat](https://delta.chat/en/download) (GNU/Linux, Android, Ubuntu
      Touch, iOS, macOS, Windows)
    - [other officially endorsed
      clients](https://support.delta.chat/t/list-of-all-know-client-projects/3059)
{{< /infobox >}}
