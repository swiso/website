---
title: Zotero
icon: icon.svg
replaces:
    - endnote
---
**Zotero** is a [free and open-source software (FOSS)][floss] that works on Linux, Mac OS and Windows. Integrations are available for LibreOffice and Microsoft Word. 

Zotero is well known for its pleasing browser extension. Not only does it recognize citation information from webpages, but also downloads the full-text PDF when available while taking a snapshot of the webpage. It is available on Firefox as well as Safari, Chrome and Edge. 

The only cost for Zotero is for additional storage. Only the storage under 2 Gb is free of charge. 

{{< infobox >}}
- **Website:**
    - [Zotero.org](https://www.zotero.org/)
- **Download:**
    - [Windows / macOS / Linux](https://www.zotero.org/download/)
    - [Android / iOS](https://www.zotero.org/support/mobile)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}