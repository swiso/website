---
title: Apertium
icon: icon.svg
replaces:
    - google-translate
---

**Apertium** is a [free software][floss] alternative available online in a web browser. Interestingly, contrary to most translators, it doesn't use a machine learning (deep learning) model, but a rule-based model.

This tool is privacy oriented. However, some specific language-to-language translations can be limited. 

Local installation is available for more advanced users. 

{{< infobox >}}
- **Website:**
    - [Apertium.com](https://apertium.org/index.eng.html)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}