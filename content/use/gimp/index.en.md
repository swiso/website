---
title: GNU Image Manipulation Program
icon: icon.svg
replaces:
    - adobe-photoshop
---

**GNU Image Manipulation Program** (short: GIMP) is one of the longest-running and best-supported [free open][floss] graphics apps. It offers Photoshop-style raster graphics and photo editing for Windows, Mac and Linux.

{{< infobox >}}
- **Website:**
    - [gimp.org](https://gimp.org/)
    - [Download](https://www.gimp.org/downloads/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
