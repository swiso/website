---
title: Guppe
icon: icon.png
replaces:
    - facebook
    - linkedin
list:
    - fediverse
---

**Guppe** allows you to create user groups where you can post using a shared
timeline, much like on [FaceBook][facebook] or [LinkedIn][linkedin].

You do not need to sign up. You can create new Guppe groups from any
ActivityPub-compatible server on the [Fediverse][fediverse].

{{< infobox >}}
- **Website:**
    - [a.gup.pe](https://a.gup.pe/)
    - [Mastodon](https://social.coop/@datatitian)
    - [Source](https://github.com/immers-space/guppe)
{{< /infobox >}}

[fediverse]: {{< relref "/articles/federated-sites" >}}
[facebook]: {{< relref "/replaces/facebook" >}}
[linkedin]: {{< relref "/replaces/linkedin" >}}
