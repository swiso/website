---
title: Flowblade
icon: icon.png
replaces:
    - adobe-premiere-pro
---

**Flowblade** is a [free and open source][floss] non-linear video
editor for Linux, available from the official website.

{{< infobox >}}
- **Website:**
    - [Flowblade](https://jliljebl.github.io/flowblade/)
    - [Download](https://jliljebl.github.io/flowblade/download.html)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
