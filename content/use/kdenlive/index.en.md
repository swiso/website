---
title: Kdenlive
icon: icon.svg
replaces:
    - adobe-premiere-pro
---

**Kdenlive** is a [free and open source][floss] non-linear video
editing app available for Windows, Mac and Linux.

{{< infobox >}}
- **Website:**
    - [kdenlive.org](https://kdenlive.org/en/)
    - [Download](https://kdenlive.org/en/download/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
