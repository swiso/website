---
title: Indico
icon: icon.png
replaces:
    - easychair
---

**Indico** is a FLOSS event management tool developed by CERN. It is also used
by the United Nations and national agencies, due to its non-profit nature. One
public, free instance is made available as a sandbox. In addition, it is
self-hostable.

{{< infobox >}}
- **Website:**
    - [getindico.io](https://getindico.io/features/)
    - [Mastodon](https://fosstodon.org/@getindico)
    - [Source](https://github.com/indico/indico)
{{< /infobox >}}
