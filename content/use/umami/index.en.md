---
title: umami
icon: icon.svg
replaces:
    - google-analytics
---

**Umami** is a simple, fast, privacy-focused alternative to Google Analytics. It is a fully [open source][floss] self-hosted web analytics solution.

{{< infobox >}}
- **Website:**
    - [umami.is](https://umami.is/)
    - [Demo](https://app.umami.is/share/8rmHaheU/umami.is)
- **Source Code:**
    - [github.com](https://github.com/umami-software/umami)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}