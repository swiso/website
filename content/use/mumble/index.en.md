---
title: Mumble
icon: icon.svg
replaces:
    - discord
---

**Mumble** is an [open-source][floss] low latency, high quality voice chat application. It also has text chats and is popular amongst gamers and podcasters.

{{< infobox >}}
- **Website:**
    - [mumble.info](https://www.mumble.info/)
    - [List of instances](https://wiki.mumble.info/wiki/Hosters)
- **Desktop app:**
    - [Windows / macOS / Linux](https://www.mumble.info/downloads/)
- **Android app:**
    - [Mumla (F-Droid)](https://f-droid.org/packages/se.lublin.mumla/)
    - [Mumla (Google Play Store)](https://play.google.com/store/apps/details?id=se.lublin.mumla) 
- **iOS app:**
    - [iOS](https://apps.apple.com/us/app/mumble/id443472808)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
