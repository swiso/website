---
title: Qwant
icon: icon.svg
replaces:
    - google-search
---

**Qwant** is a search engine based in Europe which doesn’t track users
or personalise results. Qwant Junior is a version of the search engine
which only lists child-friendly results.

{{< infobox >}}
- **Websites:**
    - [Qwant](https://www.qwant.com)
    - [Qwant Junior](https://www.qwantjunior.com/)
- **Android app:**
    - [Qwant](https://play.google.com/store/apps/details?id=com.qwant.liberty)
- **iOS app:**
    - [Qwant](https://apps.apple.com/app/qwant/id924470452)
{{< /infobox >}}
