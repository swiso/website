---
title: Shotcut
icon: icon.png
replaces: 
    - adobe-premiere-pro
---

**Shotcut** is a [free and open source][floss] non-linear video editor
for Windows, Mac and Linux, available from the official website.

{{< infobox >}}
- **Website:**
    - [shotcut.org](https://www.shotcut.org/)
    - [Download](https://www.shotcut.org/download/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
