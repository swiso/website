---
title: Open Source Games
icon: icon.png
replaces:
    - steam
---

Although they are relatively rare, there are some good games available
as [free and open source][floss] software. This means they’re
available without any kind of [DRM][drm] restrictions, and are
typically free of charge, too.

{{< infobox >}}
- **Website:**
    - [Libre Game Wiki](https://libregamewiki.org/List_of_games)
    - [Ashpex Linux Games](https://gitlab.com/Ashpex/Linux_Games)
{{< /infobox >}}

[drm]: {{< relref "/articles/digital-rights-management" >}}
[floss]: {{< relref "/articles/free-libre-open-software" >}}
