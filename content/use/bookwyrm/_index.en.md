---
title: BookWyrm
icon: logo.png
replaces:
    - amazon-goodreads
---

**Bookwyrm** is a [free and open-source][floss] and
[federated][fediverse] social reading platform where you can track
your reading, share your thoughts, and discover books without being
pestered by ads.

{{< infobox >}}
- **Website:**
    - [official site](https://joinbookwyrm.com/)
	- [list of instances](https://joinbookwyrm.com/instances/)
- **Source code:**
    - [github](https://github.com/bookwyrm-social/bookwyrm)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
[fediverse]: {{< relref "/articles/federated-sites" >}}

