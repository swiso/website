---
title: LibreWolf
icon: icon.svg
replaces:
    - google-chrome
---

**LibreWolf** is a fork of Firefox designed to minimize data collection and
telemetry as much as possible. This is achieved through hundreds of
privacy/security/performance settings and patches. Intrusive integrated addons
including updater, crashreporter, and pocket are removed too.

{{< infobox >}}
- **Website:**
    - [LibreWolf](https://librewolf.net/)
    - [Source](https://codeberg.org/librewolf)
    - [Matrix](https://matrix.to/#/#librewolf:matrix.org)
    - [Lemmy](https://lemmy.ml/c/librewolf)
{{< /infobox >}}
