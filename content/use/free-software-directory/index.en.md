---
title: Free Software Directory
icon: logo-fsf.svg
replaces:
    - switching-software
---

The [Free Software Foundation][fsf] maintains the **Free Software
Directory** which lists exclusively [libre][floss] software for
various program types in a wiki-like manner.

{{< infobox >}}
- **Website:**
    - [Free Software Directory](https://directory.fsf.org/wiki/Main_Page)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
[fsf]: https://www.fsf.org/
