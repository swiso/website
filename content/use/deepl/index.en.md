---
title: DeepL
icon: icon.svg
replaces:
    - google-translate
---

**DeepL** is a freemium translator. This software is proprietary, but the basic version is available online free of charge. Additional functions are available through the paying versions. 

This tool is know for it's quality and accuracy. Some say it is even better than Google Translate. Its translations are precise with good grammar and sentence structure. 

Similarly to Google Translate, this tool is using machine learning and is in constant training to increase its performance. This means that the users' data can be temporarily used for this training. While their privacy policy is a little bit better than Google Translate's, it is still risky. Do not use this tool for sensitive data. Additional privacy functions are available in the paying versions.  

DeepL can be used online in a web browser, as an extension or with an app. This tool can translate text and PDF documents as well. Possible integration with Microsoft Office.

{{< infobox >}}
- **Online Translator:**
    - [DeepL.com/translator](https://www.deepl.com/translator)
- **Browser Extensions:**
    - [Firefox / Chrome / Edge](https://www.deepl.com/en/app)
- **Download:**
    - [Windows / macOS](https://www.deepl.com/en/app)
    - [Android / iOS](https://www.deepl.com/mobile-apps)
    
    
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
