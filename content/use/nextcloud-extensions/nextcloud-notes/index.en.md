---
title: Nextcloud Notes
icon: icon.svg
replaces:
    - evernote
    - microsoft-onenote
    - google-keep

---

If you are using Nextcloud, the **Notes extension** is a good option for organizing your notes.
You can edit them with every Nextcloud client and many third-party apps.

{{< infobox >}}
- **Website:**
    - [Notes Extension](https://apps.nextcloud.com/apps/notes)
- **Android app:**
    - [Nextcloud Notes (F-Droid)](https://f-droid.org/packages/it.niedermann.owncloud.notes/)
    - [Nextcloud Notes (Google Play Store)](https://play.google.com/store/apps/details?id=it.niedermann.owncloud.notes)
- **iOS app:**  
    - [iOS](https://apps.apple.com/app/cloudnotes-owncloud-notes/id813973264)
{{< /infobox >}}