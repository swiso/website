---
title: Standard Notes
icon: icon.svg
replaces:
    - evernote
    - microsoft-onenote
    - google-keep
---

**Standard Notes** is a simple [open source][floss] notes app focussing on simplicity.
By creating an account, you can easily sync your notes across devices with end-to-end-encryption.

Advanced users might be interested in the paid extended version or in hosting the sync server themselves.

{{< infobox >}}
- **Website:**
    - [StandardNotes.org](https://standardnotes.org/)
    - [Web version](https://standardnotes.org/extensions?downloaded=web)
- **Android app:**
    - [Standard Notes (F-Droid)](https://f-droid.org/en/packages/com.standardnotes/)
    - [Standard Notes (Google Play Store)](https://play.google.com/store/apps/details?id=com.standardnotes)
- **iOS app:**
    - [iOS](https://apps.apple.com/us/app/standard-notes/id1285392450)
- **Desktop app:**
    - [Windows / macOS / Linux](https://standardnotes.org/#get-started)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}