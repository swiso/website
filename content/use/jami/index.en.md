---
title: Jami
icon: icon.svg
replaces:
    - whatsapp
    - facebook-messenger
    - skype
    - google-hangouts
    - zoom
    - google-meet
---

**Jami** (formerly "Ring") is a [libre][floss] easy-to-use alternative to Skype, with voice call, video call and messaging. 

It is open source and designed to respect its users’ privacy, with calls going directly from one user to another without using servers. It gives all its members a unique code, which they can share with others, so that they can add each other on the service.

{{< infobox >}}
- **Website:**
    - [Jami](https://jami.net/)
- **Android app:**
    - [Jami (F-Droid)](https://f-droid.org/en/packages/cx.ring/)
    - [Jami (Google Play Store)](https://play.google.com/store/apps/details?id=cx.ring)
- **iOS app:**
    - [iOS](https://itunes.apple.com/app/ring-a-gnu-package/id1306951055)
- **Desktop app:**
    - [Windows](https://jami.net/download-jami-windows/)
    - [macOS](https://jami.net/download-jami-macos/)
    - [Linux](https://jami.net/download-jami-linux/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}