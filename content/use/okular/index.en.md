---
title: Okular
icon: icon.png
replaces:
    - adobe-acrobat
---

**Okular** is a FLOSS PDF reader by the KDE global community. KDE's financial
and legal matters are managed by a German non-profit with no direct influence on
software development. Some of the formats Okular supports are "PDF, EPub, DjVU
and MD for documents; JPEG, PNG, GIF, Tiff, WebP for images; CBR and CBZ for
comics". It also lets you annotate and sign documents, as well as view their
table of contents.

{{< infobox >}}
- **Website:**
    - [okular.kde.org](https://okular.kde.org/)
    - [Matrix](https://go.kde.org/matrix/#/#kde:kde.org)
    - [Mastodon](https://floss.social/@kde)
    - [Source](https://invent.kde.org/graphics/okular)
{{< /infobox >}}
