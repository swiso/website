---
title: DuckDuckGo
icon: icon.svg
replaces:
    - google-search
---

The most popular and easiest to use privacy-conscious search engine, **DuckDuckGo** is funded by advertising but does not track its users. It is based in the USA.

You can search through the website just like you would on Google, and there are phone apps available that provide additional protection when browsing the web. You can also set your web browser to use DuckDuckGo as its default search engine.

{{< infobox >}}
- **Website:** 
    - [DuckDuckGo](https://duckduckgo.com/)
- **Android app:** 
    - [DuckDuckGo Privacy Browser (F-Droid)](https://f-droid.org/en/packages/com.duckduckgo.mobile.android/)
    - [DuckDuckGo Privacy Browser (Google Play Store)](https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android)
- **iOS app:** 
    - [DuckDuckGo Privacy Browser](https://apps.apple.com/app/duckduckgo-search-stories/id663592361)
{{< /infobox >}}